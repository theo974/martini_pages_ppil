#pragma once
#include <vector>
#include <iostream>
#include <string>
#include "Forme.h"

class Groupe {
private:
    vector<Forme*> _groupe;
    string _couleur;
    string _nom;

public:
    Groupe(string nom) {
        _nom = nom;
        _couleur = "BLACK";
    }

    Groupe(string nom, string couleur) {
        _nom = nom;
        _couleur = couleur;
    }

    Groupe(Forme* forme) {
        _groupe.push_back(forme);
        _couleur = "BLACK";
    }

    Groupe(Forme* forme, string couleur) {
        _groupe.push_back(forme);
        _couleur = couleur;
    }

    ~Groupe() {
        for (vector<Forme*>::iterator i = _groupe.begin(); i != _groupe.end(); ++i) {
            free(*i);
        }
    }

    virtual void ajouter(Forme* forme) {
        _groupe.push_back(forme);
    }

    virtual void ajouter(vector<Forme*> liste) {
        for (vector<Forme*>::iterator i = liste.begin(); i != liste.end(); ++i) {
            _groupe.push_back(*i);
        }
    }

    void afficher(){
        cout << _nom << endl;
        for (vector<Forme*>::iterator i = _groupe.begin(); i != _groupe.end(); ++i) {
            cout << *(*i);
        }
        cout << "fin " << _nom << endl;
    }

    // TRANSFORMATION

    //AIRE
    double CalculeAire() {
        double aire = 0;
        for (vector<Forme*>::iterator i = _groupe.begin(); i != _groupe.end(); ++i) {

        }
        return 0;
    }

    /*const void accepte(const VisiteurForme* visiteurForme) const{
        visiteurForme->visite(this);
    }*/

};
