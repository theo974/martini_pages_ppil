// TEST1_PPIL.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//
#include <iostream>
#include <string>

#include "Forme.h"
#include "Vecteur2D.h"
#include "Triangle.h"
#include "Cercle.h"
#include "Segment.h"
#include "Groupe.h"
#include "Connection.h"

#include "VisiteurForme.h"
#include "VisiteurFormeDessinJAVA.h"
#include <math.h>


using namespace std;

int main()
{
    cout << "Hello World!\n";

    ///---------- TEST VECTEUR ----------
    cout << "---------- TEST VECTEUR ----------" << endl;
    Vecteur2D u1(20, 30), u2(10, 30), v(5), w, v1(35, -63), u3(30, 40), u4(3, -4), v3;
    cout << " u1 = " << u1 << endl;
    cout << " u2 = " << u2 << endl;
    cout << " u1 - u2 = " << u1 - u2 << endl;
    cout << " u1 + u2 = " << u1 + u2 << endl;
    cout << " 5*u1 = " << 5 * u1 << endl << endl << endl;


    ///---------- TEST TRANSFORMATION POINT ----------
    cout << "---------- TEST TRANSFORMATION POINT ----------" << endl;

    u1 = translationPoint(u1, u2);
    cout << "translation : " << u1 << endl;
    u1.x = 20;
    u1.y = 30;
    u1 = rotationPoint(u1, u2, 0);
    cout << "rotation : " << u1 << endl;
    u1.x = 20;
    u1.y = 30;
    u1 = homothetiePoint(u1, u2, 2);
    cout << "homothetie : " << u1 << endl << endl << endl;
    u1.x = 20;
    u1.y = 30;



    ///---------- TEST TRIANGLE ----------
    cout << "---------- TEST TRIANGLE ----------" << endl;
    Triangle T1("T1", u1, u2, u3);
    cout << T1;
    //test methode calcul aire
    double u = T1.CalculAire();
    cout << "aire de " << T1._nom << " : " << u << endl;
    //test transformation
    T1.translation(u1);
    cout << T1;
    //T1.rotation(u1, 2*M_PI);
    cout << T1;
    cout << T1.visite() << endl << endl;



    ///---------- TEST SEGMENT ----------
    cout << "---------- TEST SEGMENT ----------" << endl;
    Segment S("segment_1", u1, u2);
    Segment* S2 = new Segment("segment_2P", u2, u3);
    cout << S;
    cout << *S2;
    //test methode calcul aire
    u = S.CalculAire();
    double a = (*S2).CalculAire();
    cout << "aire de " << S._nom << " : " << u << endl;
    cout << "aire de " << (*S2)._nom << " : " << a << endl;
    cout << S.visite() << endl << endl;



    ///---------- TEST CERCLE ----------
    cout << "---------- TEST CERCLE ----------" << endl;
    Cercle C("Cercle_1", u1, 40);
    cout << C;
    //test methode calcul aire
    u = C.CalculAire();
    cout << "aire de " << S._nom << " : " << u << endl;
    C.translation(u1);
    cout << C;
    //C.rotation(u1, 2 * M_PI);
    cout << C;
    cout << C.visite() << endl << endl;


    /// TEST COORDO TO PIXEL
    cout << "---------- TEST COORDO TO PIXEL ----------" << endl;
    Vecteur2D u5(3, 11.5), u6(-8, 69), u7(42, -20);
    vector<Vecteur2D> V;
    V.push_back(u5);
    V.push_back(u6);
    V.push_back(u7);
    cout << "Avant la transformation (en coordonnees) :" << endl;
    for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
        cout << (*i);
    }
    cout << endl;

    V = coordoToPixel(V);
    cout << "Apres la transformation (en pixel) :" << endl;
    for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
        cout << (*i);
    }
    cout << endl << endl << endl;


    ///---------- TEST GROUPE ----------
    cout << "---------- TEST GROUPE ----------" << endl;
    Groupe G("G1", "BLACK");
    Segment* S1 = new Segment("S1", u1, u2);
    G.ajouter(S1);
    G.afficher();
    Segment* S3 = new Segment("S3", u2, u3);
    G.ajouter(S3);
    G.afficher();
    //test methode calcul aire
    u = C.CalculAire();
    Cercle C1 = C;
    cout << "aire de " << C._nom << " : " << u << endl;
    C1.translation(u1);
    cout << C;
    C1.rotation(u1, 2 * M_PI);
    cout << C << endl << endl;


    ///---------- TEST CONNEXION ----------
    cout << "---------- TEST CONNEXION ----------" << endl;

    vector<Vecteur2D> vec2;
    vec2.push_back(C.getO());
    vec2.push_back(C.getP());
    vec2.push_back(S.getA());
    vec2.push_back(S.getB());
    vec2.push_back(T1.getA());
    vec2.push_back(T1.getB());
    vec2.push_back(T1.getC());

    vec2 = coordoToPixel(vec2);

    cout << C.visite();
    C.setO(vec2[0]);
    C.setRayon(vec2[1]);
    cout << C.visite();

    cout << S.visite();
    S.setA(vec2[2]);
    S.setB(vec2[3]);
    cout << S.visite();

    cout << T1.visite();
    T1.setA(vec2[4]);
    T1.setB(vec2[5]);
    T1.setC(vec2[6]);
    cout << T1.visite();

    vector<string> V1;
    V1.push_back(C.visite());
    V1.push_back(S.visite());

    int abc = connexion(V1);
}
