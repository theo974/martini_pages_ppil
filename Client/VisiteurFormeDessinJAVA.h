#pragma once

#include "Forme.h"
#include "VisiteurForme.h"
#include "Connection.h"
#include <vector>
//#ifdef _WIN32
//#include <winsock2.h>
//#else
//#include <sys/socket.h>
//#include <arpa/inet.h>
//#include <netdb.h>
//#include <unistd.h>

/// UTILISER CONNECTION ET DESSIN PUIS TESTER SI CA MARCHE

// D ABORD ON CREE LA CONNEXION ET ON TESTE AVEC DRAW;...
// PUIS ON VERRA APRES DESSIN

//class VisiteurForme{};

class VisiteurFormeDessinJAVA : public VisiteurForme {

public:
    //VisiteurFormeDessinJAVA(){}

    virtual void visite(Cercle* forme) {
        string s;
        const int taille = 4;
        double t[taille] = { forme->getO().x,forme->getO().y, forme->getO().x + forme->getRayon(), forme->getO().y };
        s = chaine("CERCLE", t, taille, forme->getCouleur());
        //envoyerRequete(_sock, aux);
    }

    /*virtual const void visite(Polygone* forme) {

    }*/

    virtual void visite(Segment* forme) {
        string s;
        const int taille = 4;
        double t[taille] = { forme->getA().x, forme->getA().y, forme->getB().x, forme->getB().y };
        s = chaine("DRAW", t, taille, forme->getCouleur());
        //envoyerRequete(_sock, aux);
    }

    virtual void visite(Triangle* forme) {
        string s;
        const int taille = 6;
        double t[taille] = { forme->getA().x, forme->getA().y, forme->getB().x, forme->getB().y, forme->getC().x, forme->getC().y };
        s = chaine("DRAW", t, taille, forme->getCouleur());
        //envoyerRequete(_sock, aux);
    }

    /*virtual const void visite(Groupe* groupe) {

    }*/

    // renvoie une chaine de coordonnes
    string prechaine(double tab[], int taille) {
        if (taille == 0) {
            return "";
        }
        else
        {
            int i;
            string s;

            s = to_string(tab[0]);

            for (i = 1; i < taille; ++i) {
                s += ";" + to_string(tab[i]);
            }
            s += "\n";
            return s;
        }
    }

    //renvoie la chaine voulue pour le serveur
    string chaine(string mode, double tab[], int taille, string couleur) {
        return mode + ";" + to_string(taille / 2) + ";" + couleur + ";" + prechaine(tab, taille);
    }
};
