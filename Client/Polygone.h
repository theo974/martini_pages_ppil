#pragma once

#include "Forme.h"
#include <math.h> // pour abs() dans la methode determinant


class Polygone : public Forme {
private:
    int _NbPoints;
    Vecteur2D* _TabPoints = (Vecteur2D*)malloc(_NbPoints * sizeof(Vecteur2D));

public:
    //constructeur avec couleur
    Polygone(string nom, Vecteur2D P[], int NbPoints, string couleur) : _NbPoints(NbPoints), Forme(nom, couleur) {
        for (int i = 0; i < NbPoints; i++) {
            _TabPoints[i] = P[i];
        }
    }

    //construcetur sans couleur
    Polygone(string nom, Vecteur2D P[], int NbPoints) : _NbPoints(NbPoints), Forme(nom) {
        for (int i = 0; i < NbPoints; i++) {
            _TabPoints[i] = P[i];
        }
    }

    virtual ~Polygone() {
        free(_TabPoints);
    }



    operator string() const {
        ostringstream o;
        o << "Polygone|" << _nom << "|" << _NbPoints << "|";
        for (int i = 0; i < _NbPoints; i++) {
            o << _TabPoints[i] << "|";
        }
        o << _couleur << "|" << endl;
        return o.str();
    }

    // A faire
    double CalculAire() {
        return 0;
    }


    //tansformations
    virtual void translation(Vecteur2D V) {
        for (int i = 0; i < _NbPoints; i++) {
            _TabPoints[i] = translationPoint(V, _TabPoints[i]);
        }
    }

    virtual void homothetie(Vecteur2D V, double k) {
        for (int i = 0; i < _NbPoints; i++) {
            _TabPoints[i] = homothetiePoint(V, _TabPoints[i], k);
        }
    }

    virtual void rotation(Vecteur2D V, double teta) {
        for (int i = 0; i < _NbPoints; i++) {
            _TabPoints[i] = rotationPoint(V, _TabPoints[i], teta);
        }
    }

    int getNbPoints() {
        return _NbPoints;
    }

    Vecteur2D* getPoints() const {
        return _TabPoints;
    }

    virtual string visite() {
        string s;
        const int taille = this->getNbPoints() * 2;
        int* t = (int*)malloc(taille * sizeof(int));
        for (int i = 0; i < this->getNbPoints(); i++) {
            t[i * 2] = this->getPoints()[i].x;
            t[i * 2 + 1] = this->getPoints()[i].y;
        }
        s = chaine("polygone", t, taille, this->getCouleur());

    }
    
    /*const void accepte(const VisiteurForme* visiteurForme) const{
        visiteurForme->visite(this);
    }*/
};
