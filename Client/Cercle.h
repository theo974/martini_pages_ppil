#pragma once
#include "Forme.h"
#include <math.h>
//#include "VisiteurForme.h"

//class VisiteurForme;

class Cercle : public Forme {
private:
    Vecteur2D _O;
    //Vecteur2D _P;
    double _rayon;

public:
    //constructeur avec couleur
    Cercle(string nom, Vecteur2D O, double rayon, string couleur) : _O(O), _rayon(rayon), Forme(nom, couleur) {}

    Cercle(string nom, double xO, double yO, double rayon, string couleur) : _O(xO, yO), _rayon(rayon), Forme(nom, couleur) {}


    //construcetur sans couleur
    Cercle(string nom, Vecteur2D O, double rayon) : _O(O), _rayon(rayon), Forme(nom) {}

    Cercle(string nom, double xO, double yO, double rayon) : _O(xO, yO), _rayon(rayon), Forme(nom) {}

    //getter
    Vecteur2D getO() {
        return _O;
    }

    double getRayon() {
        return _rayon;
    }

    Vecteur2D getP() {
        Vecteur2D P;
        P.x = _O.x + _rayon;
        P.y = _O.y;
        return P;
    }

    //Setters
    void setO(Vecteur2D vec) {
        _O = vec;
    }

    void setRayon(Vecteur2D vec) {
        int getDifferenceX = abs(vec.x - _O.x);
        int getDifferenceY = abs(vec.y - _O.y);
        int hypothenuse = (int) ceil(sqrt(pow(getDifferenceX, 2) + pow(getDifferenceY, 2)));
        _rayon = hypothenuse;
    }

    virtual ~Cercle() {}

    operator string() const {
        ostringstream o;
        o << "Cercle|" << _nom << "|" << _O << "|" << _rayon << "|" << _couleur << "|" << endl;
        return o.str();
    }

    double CalculAire() {
        double aire = 2 * 3.14 * _rayon * _rayon;
        return aire;
    }


    //tansformations
    virtual void translation(Vecteur2D V) {
        _O = translationPoint(V, _O);
    }

    virtual void homothetie(Vecteur2D V, double k) {
        _O = homothetiePoint(_O, V, k);
    }

    virtual void rotation(Vecteur2D V, double teta) {
        _O = rotationPoint(_O, V, teta);
    }

    /*void accepte(VisiteurForme* v){
        v->visite(this);
    }*/

    virtual string visite() {
        string s;
        const int taille = 4;
        int t[taille] = { (int) this->getO().x, (int) this->getO().y, (int) this->getO().x + (int) this->getRayon(), (int) this->getO().y };
        s = chaine("CERCLE", t, taille, this->getCouleur());
        return s;
        //envoyerRequete(_sock, aux);
    }
};
