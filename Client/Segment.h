#pragma once
#include "Forme.h"
//#include "VisiteurForme.h"

//class VisiteurForme;

class Segment : public Forme {
private:
    Vecteur2D _A, _B;

public:
    //constructeur avec couleur
    Segment(string nom, Vecteur2D A, Vecteur2D B, string couleur) : _A(A), _B(B), Forme(nom, couleur) {}

    Segment(string nom, double xA, double yA, double xB, double yB, string couleur) : _A(xA, yA), _B(xB, yB), Forme(nom, couleur) {}

    //construcetur sans couleur
    Segment(string nom, Vecteur2D A, Vecteur2D B) : _A(A), _B(B), Forme(nom) {}

    Segment(string nom, double xA, double yA, double xB, double yB) : _A(xA, yA), _B(xB, yB), Forme(nom) {}


    // getter
    Vecteur2D getA() {
        return _A;
    }

    Vecteur2D getB() {
        return _B;
    }

    void setA(Vecteur2D v) {
        _A = v;
    }

    void setB(Vecteur2D v) {
        _B = v;
    }

    virtual ~Segment() {}

    operator string() const {
        ostringstream o;
        o << "Segment|" << _nom << "|" << _A << "|" << _B << "|" << _couleur << "|" << endl;
        return o.str();
    }

    double CalculAire() { //Idiot de faire ca ?
        return 0;
    }

    ///definir une methode qui calcule la longueur en plus (peut etre)

    //tansformations

    virtual void translation(Vecteur2D V) {
        _A = translationPoint(V, _A);
        _B = translationPoint(V, _B);
    }

    virtual void homothetie(Vecteur2D V, double k) {
        _A = homothetiePoint(_A, V, k);
        _B = homothetiePoint(_B, V, k);
    }

    virtual void rotation(Vecteur2D V, double teta) {
        _A = rotationPoint(_A, V, teta);
        _B = rotationPoint(_B, V, teta);
    }

    /*void accepte(VisiteurForme* v){
        v->visite(this);
    }*/

    virtual string visite() {
        string s;
        const int taille = 4;
        int t[taille] = { (int) this->getA().x, (int) this->getA().y, (int) this->getB().x, (int) this->getB().y };
        s = chaine("POLY", t, taille, this->getCouleur());
        return s;
        //envoyerRequete(_sock, aux);
    }
};
