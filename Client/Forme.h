#pragma once

#include <string>
#include "Vecteur2D.h"
#include "Transformation.h"
//#include "VisiteurForme.h";

class VisiteurForme;


class Forme {
public:
    string _nom;
    string _couleur;

    //constructeurs
    Forme(string nom) : _nom(nom), _couleur("BLACK") {}
    Forme(string nom, string couleur) : _nom(nom), _couleur(couleur) {}

    //destructeurs
    virtual ~Forme() {}

    //getter/setter
    string getCouleur()const { return _couleur; }
    void setCouleur(string couleur) { _couleur = couleur; }
    string getNom()const { return _nom; }
    void setNom(const string nom) { _nom = nom; }

    //calcul aire
    virtual double CalculAire() = 0;

    virtual operator string() const = 0;



    /*
    virtual void dessin() const = 0;
    */
    // OBligatoirement virtuelles pures ? (à voir)
    //
    //
    //
    //
    //
    // LES TRANSFORMATIONS NE CREENT PAS DE NOUVEAUX POINTS, POUR L INSTANT ELLES NE CHANGENT QUE LES POINTS DEJA EXISTANTS
    virtual void translation(Vecteur2D V) = 0;
    virtual void homothetie(Vecteur2D V, double k) = 0;
    virtual void rotation(Vecteur2D V, double teta) = 0;

    //virtual void accepte(VisiteurForme* ) = 0;
};
ostream& operator << (ostream& os, const Forme& f) {
    os << (string)f;
    return os;
}
