#pragma once

#include "Forme.h"
#include "Cercle.h"
#include "Polygone.h"
#include "Segment.h"
#include "Triangle.h"
#include "Groupe.h"


class VisiteurForme {
public:

    virtual void visite(Cercle* forme) = 0;

    //virtual const void visite(Polygone* forme) = 0;

    virtual void visite(Segment* forme) = 0;

    virtual void visite(Triangle* forme) = 0;

    //virtual const void visite(Groupe* groupe) = 0;
};
