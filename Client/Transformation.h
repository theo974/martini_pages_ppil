﻿#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <valarray>
#include "Vecteur2D.h"

//point est fixe
Vecteur2D translationPoint(Vecteur2D V, Vecteur2D point) {
    /*V.x = V.x + point.x;
    V.y = V.y + point.y;*/
    return V + point;
}

//point est fixe dans le plan et les coordonnées de V varient
Vecteur2D homothetiePoint(Vecteur2D V, Vecteur2D point, double z) {
    if (z == 0) {
        printf("ERRRRRRRRRRRRRRRRRRRRRRRRRROR k == 0");
        exit(1);
    }
    else {
        if (point.x > V.x) {
            V.x = point.x + z * (point.x - V.x);
        }
        else if (point.x < V.x) {
            V.x = point.x - z * (V.x - point.x);
        }

        if (point.y > V.y) {
            V.y = point.y - z * (point.y - V.y);
        }
        else if (point.y < V.y) {
            V.y = point.y + z * (V.y - point.y);
        }
    }
    return V;
}

// V change, point est fixe
Vecteur2D rotationPoint(Vecteur2D V, Vecteur2D point, double teta) {
    // Les nouvelles coordonn�es seront V.x = ||Vpoint|| cos(teta) et V.y = ||Vpoint|| sin(teta)
    V.x = cos(teta) * sqrt((V.x - point.x) * (V.x - point.x) + (V.y - point.y) * (V.y - point.y));
    V.y = sin(teta) * sqrt((V.x - point.x) * (V.x - point.x) + (V.y - point.y) * (V.y - point.y));
    return V;
}



//LES DEUX FONCTIONS SUIVANTES SONT ISSUES DU FICHIER VisiteurFormeDessinJAVA.h
// renvoie une chaine de coordonnées
string prechaine(int tab[], int taille) {
    if (taille == 0) {
        return "";
    }
    else
    {
        int i;
        string s;

        s = to_string(tab[0]);

        for (i = 1; i < taille; ++i) {
            s += ";" + to_string(tab[i]);
        }
        s += "\n";
        return s;
    }
}

//renvoie la chaine voulue pour le serveur
string chaine(string mode, int tab[], int taille, string couleur) {
    return mode + ";" + to_string(taille / 2) + ";" + couleur + ";" + prechaine(tab, taille);
}




vector<Vecteur2D> coordoToPixel(vector<Vecteur2D> V) {
    int negative = 0;
    double decalage, coefficient;
    //On regarde si on a des coordonnées négatives
    for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
        if (i->x < 0 || i->y < 0) {
            negative = 1;
            break;
        }
    }
    //Si oui, on fait une translation
    if (negative) {
        double min = 1000;
        //On cherche la plus petite coordonnée
        for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
            if (i->x < min) min = i->x;
            if (i->y < min) min = i->y;
        }
        decalage = abs(min);
        //On ajoute la valeur absolue de la plus petite coordonnées pour tout avoir en positif
        for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
            i->x = (i->x) + decalage;
            i->y = (i->y) + decalage;
        }
    }
    //On cherche ensuite la plus grande coordonnée
    double max = 0;
    for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
        if (i->x > max) max = i->x;
        if (i->y > max) max = i->y;
    }
    //On determine le coefficient multiplicateur pour un bon affichage (arrondi + 1)
    coefficient = trunc(1000 / max);
    //On multiplie toutes les coordonnées par se coefficient, qu'on arrondi ensuite pour avoir des pixels
    for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
        i->x = round((i->x) * coefficient);
        i->y = round((i->y) * coefficient);
    }
    //On inverse ensuite toutes les coordonnées Y (car notre repère des ordonnées vas du haut vers le bas
    for (vector<Vecteur2D>::iterator i = V.begin(); i != V.end(); ++i) {
        i->y = 1000 - i->y;
    }
    return V;
}
