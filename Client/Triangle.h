#pragma once

#include "Forme.h"
//#include "VisiteurForme.h"
#include <math.h> // pour abs() dans la methode determinant

//class VisiteurForme;

class Triangle : public Forme {
private:
    Vecteur2D _A, _B, _C;
    //VisiteurForme* v1;

public:
    //constructeur avec couleur
    Triangle(string nom, Vecteur2D A, Vecteur2D B, Vecteur2D C, string couleur) : _A(A), _B(B), _C(C), Forme(nom, couleur) {}

    Triangle(string nom, double xA, double yA, double xB, double yB, double xC, double yC, string couleur) : _A(xA, yA), _B(xB, yB), _C(xC, yC), Forme(nom, couleur) {}


    //construcetur sans couleur
    Triangle(string nom, Vecteur2D A, Vecteur2D B, Vecteur2D C) : _A(A), _B(B), _C(C), Forme(nom) {}

    Triangle(string nom, double xA, double yA, double xB, double yB, double xC, double yC) : _A(xA, yA), _B(xB, yB), _C(xC, yC), Forme(nom) {}

    //getter
    Vecteur2D getA() {
        return _A;
    }

    Vecteur2D getB() {
        return _B;
    }

    Vecteur2D getC() {
        return _C;
    }

    //setter
    void setA(Vecteur2D vec) {
        _A = vec;
    }

    void setB(Vecteur2D vec) {
        _B = vec;
    }

    void setC(Vecteur2D vec) {
        _C = vec;
    }

    virtual ~Triangle() {}

    //virtual Forme* clone() const { return new Triangle(*this); }

    operator string() const {
        ostringstream o;
        o << "Triangle|" << _nom << "|" << _A << "|" << _B << "|" << _C << "|" << _couleur << "|" << endl;
        return o.str();
    }

    double CalculAire() {
        Vecteur2D* v1 = new Vecteur2D(_B.x - _A.x, _B.y - _A.y);
        Vecteur2D* v2 = new Vecteur2D(_C.x - _A.x, _C.y - _A.y);
        Vecteur2D* v3 = new Vecteur2D();
        //return 0.5*v1->Det(v2);
        return abs((v3->Determinant(v1, v2)) / 2);
    }


    //tansformations
    virtual void translation(Vecteur2D U) {
        _A = translationPoint(_A, U);
        _B = translationPoint(_B, U);
        _C = translationPoint(_C, U);
    }

    virtual void homothetie(Vecteur2D V, double k) {
        _A = homothetiePoint(_A, V, k);
        _B = homothetiePoint(_B, V, k);
        _C = homothetiePoint(_C, V, k);
    }

    virtual void rotation(Vecteur2D V, double teta) {
        _A = rotationPoint(_A, V, teta);
        _B = rotationPoint(_B, V, teta);
        _C = rotationPoint(_C, V, teta);
    }

    /*void accepte(VisiteurForme* v) {
        v->visite(this);
    }*/

    virtual string visite() {
        string s;
        const int taille = 6;
        int t[taille] = { (int) this->getA().x, (int) this->getA().y, (int) this->getB().x, (int) this->getB().y, (int)this->getC().x, (int) this->getC().y };
        s = chaine("POLY", t, taille, this->getCouleur());
        return s;
        //envoyerRequete(_sock, aux);
    }
};
