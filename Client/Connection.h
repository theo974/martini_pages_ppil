#pragma once
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring> // pour convertir une string en chaine de caracteres
#include <string.h>
#include "Erreur.h" // ma petite classe d'erreur personnelle

#pragma comment(lib, "ws2_32.lib") // sp�cifique � VISUAL C++

using namespace std;

//#define L 200 // longueur max d'une cha�ne de caract�res
//#define _WINSOCK_DEPRECATED_NO_WARNINGS //pour eviter une erreur � la ligne 69
#define _CRT_SECURE_NO_WARNINGS

int connexion(vector<string> V) {
    try
    {
        //-------------- initialisation ressources librairie winsock------------------------------ -
        int r;
        WSADATA wsaData; // structure contenant les donn�es de la librairie winsock � initialiser
        r = WSAStartup(MAKEWORD(2, 0), &wsaData); // MAKEWORD(2,0) sert � indiquer la version de la librairie � utiliser : 1 pour winsock et 2 pour winsock2
        // en cas de succ�s, wsaData a �t� initialis�e et l'appel a renvoy� la valeur 0
        if (r) throw Erreur("L'initialisation a �chou�");
        cout << "client vers le serveur de majuscule" << endl;
        cout << "initialisation effectu�e" << endl;

        //---------------------- cr�ation socket------------------------------------------------ -
        SOCKET sock; // informations concernant le socket � cr�er : famille d'adressesaccept�es, mode connect� ou non, protocole
        int familleAdresses = AF_INET; // IPv4
        int typeSocket = SOCK_STREAM; // mode connect� TCP
        int protocole = IPPROTO_TCP; // protocole. On peut aussi mettre 0 et la fct choisit le protocole en fct des 2 1ers param�tres
        // pour les valeurs des param�tres : cf. fct socket dans la doc sur winsock
        sock = socket(familleAdresses, typeSocket, protocole);
        if (sock == INVALID_SOCKET)
        {
            ostringstream oss;
            oss << "la cr�ation du socket a �chou� : code d'erreur = " << WSAGetLastError() <<
                endl; // pour les valeurs renvoy�es par WSAGetLastError() : cf. doc r�f winsock
            throw Erreur(oss.str().c_str());
        }
        cout << "socket cr��" << endl;

        //------------------------------ cr�ation du repr�sentant du serveur----------------------------------------
        static const int L = 300;
        char adresseServeur[L];
        short portServeur;
        cout << "tapez l'adresse IP du serveur de majuscule : " << endl;
        cin >> adresseServeur;
        cout << "tapez le port du serveur du serveur de majuscule : " << endl;
        cin >> portServeur;
        SOCKADDR_IN sockaddr; // informations concernant le serveur avec lequel on va communiquer
        sockaddr.sin_family = AF_INET;
        //sockaddr.sin_addr.s_addr = inet_addr(adresseServeur); // inet_addr() convertit de l'ASCII en entier
        long i = inet_pton(AF_INET, adresseServeur, &sockaddr.sin_addr.s_addr);
        /*
        INT WSAAPI inet_pton(
        [in]  INT   Family,
[in]  PCSTR pszAddrString,
[out] PVOID pAddrBuf
);
            */
        if (i != 1) { throw Erreur("inet_pton échoue"); }
        sockaddr.sin_port = htons(portServeur); //htons() assure que le port est bien inscrit dans le format du r�seau(little - endian ou big - endian)

        //-------------- connexion du client au serveur-------------------------------------- -
        r = connect(sock, (SOCKADDR*)&sockaddr, sizeof(sockaddr)); // renvoie une valeur non nulle en cas d'�chec.
        // Le code d'erreurpeut �tre obtenu par un appel � WSAGetLastError()
        if (r == SOCKET_ERROR)
            throw Erreur("La connexion a échoué");
        cout << "connexion au serveur de dessin réussie" << endl;
        //bool continuer;

        for (int i = 0; i<V.size(); i++)
        {
            char requete[L];
            char* q;

            q = (char*)malloc(sizeof(char) * L);
            q = (char*)V[i].c_str();

            cout << q;

            //strcpy_s(p, V[i].c_str());

            cout << "Tapez la chaine pour le dessin ou tapez \"quitter\" : ";

            //strcat_s(q, L, "\r\n"); // pour que le serveur r�ceptionne bien le message
            int l = strlen(q);
            r = send(sock, q, l, 0); //------------------ envoi de la requ�te au serveur------------------------------ -
            // envoie au plus l octets
            if (r == SOCKET_ERROR)
                throw Erreur("�chec de l'envoi de la requ�te");
            char reponse[L];
            r = recv(sock, reponse, l, 0); //----------- r�ception de la r�ponse du serveur---------------------------- -
            // re�oit au plus l octets
            // en cas de succ�s, r contient le nombre d'octets re�us
            if (r == SOCKET_ERROR)
                throw Erreur("La r�ception de la r�ponse a �chou�");
            char* p = strchr(reponse, '\n');
            //*p = '\0';
            //cout << reponse << endl;

        }
        r = shutdown(sock, SD_BOTH); // on coupe la connexion pour l'envoi et la r�ception
        // renvoie une valeur non nulle en cas d'�chec. Le code d'erreur peut �tre obtenu par un appel � WSAGetLastError()
        if (r == SOCKET_ERROR)
            throw Erreur("la coupure de connexion a �chou�");
        r = closesocket(sock); // renvoie une valeur non nulle en cas d'�chec. Le code d'erreur peut �tre obtenu par un appel � WSAGetLastError()
        if (r) throw Erreur("La fermeture du socket a �chou�");
        WSACleanup();
        cout << "arr�t normal du client" << endl;
    }
    catch (Erreur erreur)
    {
        cerr << erreur << endl;
    }
    return 0;
}