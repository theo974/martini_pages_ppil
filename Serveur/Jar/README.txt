Pour utiliser PPIL_ServeurJAVA.jar (Utilisable également via le code source) :

Version de JAVA requise : 17.0.1

- Lancer le .jar, il tournera ensuite en arrière plan.
- Une fenêtre de dessin s'ouvrira lorsqu'une connexion avec un client s'effectuera.
- Pour fermer l'application, il faut lui forcer l'arrêt via le Gestionnaire des tâches.