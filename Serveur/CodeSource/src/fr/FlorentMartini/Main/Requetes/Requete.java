package fr.FlorentMartini.Main.Requetes;

import fr.FlorentMartini.Main.GUI.MainGUI;

public abstract class Requete {
    //Champs
    private String TypeDeRequete; //DRAW or ROTATION or...
    private fr.FlorentMartini.Main.Color couleur; //Couleur de dessin
    private int nbPoints = 0; //Nombre de côtés / coin si requête = DRAW
    private int[] positions = new int[40]; //Les Coordonnées sur requête = DRAW

    //Constructeur
    public Requete(String TypeDeRequete, fr.FlorentMartini.Main.Color couleur, String nbPoints, String[] position) throws IllegalArgumentException {
        this.TypeDeRequete = TypeDeRequete;
        this.couleur = couleur;
        if(Integer.parseInt(nbPoints)<2) throw new IllegalArgumentException("NbPoints insuffisant");
        this.nbPoints = Integer.parseInt(nbPoints);
        for(int i=0; i<(this.nbPoints*2); i++) {
            this.positions[i] = Integer.parseInt(position[i]);
        }
    }

    //Getters
    public int getNbCotes() { return nbPoints; }
    public fr.FlorentMartini.Main.Color getCouleur() { return couleur; }
    public String getTypeDeRequete() { return TypeDeRequete; }
    public int[] getPositions() { return positions; }

    //Méthodes
    public abstract void actionRequete(MainGUI affichage);
    public abstract String toString();
}
