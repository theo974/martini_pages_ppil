package fr.FlorentMartini.Main.Requetes;

import fr.FlorentMartini.Main.Color;
import fr.FlorentMartini.Main.GUI.DrawCercle;
import fr.FlorentMartini.Main.GUI.MainGUI;

public class RequeteCercle extends Requete {
    public RequeteCercle(String TypeDeRequete, Color couleur, String[] position) throws IllegalArgumentException {
        super(TypeDeRequete, couleur, "2", position);
    }

    @Override
    public void actionRequete(MainGUI affichage) {
        affichage.add(new DrawCercle(this));
        affichage.setVisible(true);
    }

    @Override
    public String toString() {
        return getTypeDeRequete() + " -> [" + getCouleur().toString() + "]";
    }
}
