package fr.FlorentMartini.Main.Requetes;

import fr.FlorentMartini.Main.Color;
import fr.FlorentMartini.Main.GUI.DrawPolygone;
import fr.FlorentMartini.Main.GUI.MainGUI;

public class RequetePolygone extends Requete {

    //Constructeur
    public RequetePolygone(String TypeDeRequete, Color couleur, String nbPoints, String position[]) throws IllegalArgumentException {
        super(TypeDeRequete, couleur, nbPoints, position);
    }

    @Override
    public void actionRequete(MainGUI affichage) {
        affichage.add(new DrawPolygone(this));
        affichage.setVisible(true);
    }

    @Override
    public String toString() {
        return getTypeDeRequete() + " -> [" + getNbCotes() + " côtés | " + getCouleur().toString() + "]";
    }
}
