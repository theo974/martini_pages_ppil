package fr.FlorentMartini.Main.Parser;

import fr.FlorentMartini.Main.Requetes.Requete;
import fr.FlorentMartini.Main.Requetes.RequetePolygone;

import java.util.ArrayList;

public abstract class ParserRequete {
    private ParserRequete suivant = null;

    public ParserRequete(ParserRequete suivant) {
        this.suivant = suivant;
    }

    public void traiter(String requete, ArrayList<Requete> resultat) throws ParserManquantException {
        if(saitParser(requete)) {
            try {
                resultat.add(parser(requete));
            } catch(Exception e) {
                System.err.println(e.getMessage());
            }
        } else if(aUnSuivant()) {
            getSuivant().traiter(requete, resultat);
        } else {
            throw new ParserManquantException();
        }
    }

    private ParserRequete getSuivant() {
        return suivant;
    }

    private boolean aUnSuivant() {
        return suivant != null;
    }

    public abstract Requete parser(String requete) throws Exception;

    public abstract boolean saitParser(String requete);
}
