package fr.FlorentMartini.Main.Parser;

import fr.FlorentMartini.Main.Color;
import fr.FlorentMartini.Main.Requetes.RequetePolygone;

public class ParserRequetePolygone extends ParserRequete {

    public ParserRequetePolygone(ParserRequete suivant) {
        super(suivant);
    }

    @Override
    public RequetePolygone parser(String requete) {
        String[] coordonnees = new String[40];
        String[] position = requete.split(";");
        Color couleur = Color.valueOf(position[2]);
        for(int i=3; i<position.length; i++) {
            coordonnees[i-3] = position[i];
        }
        return new RequetePolygone(position[0], couleur, position[1], coordonnees);
    }

    @Override
    public boolean saitParser(String requete) {
        return requete.contains("POLY");
    }
}
