package fr.FlorentMartini.Main.Parser;

/**
 * Class héritière de la class Exception qui intervient si aucun maillon dans le Parser peut parser la ligne reçu.
 * @author MARTINI Florent
 */
@SuppressWarnings("serial")
public class ParserManquantException extends Exception {
    public ParserManquantException() {
        super("Aucun parser ne peut lire cette requete");
    }

}

