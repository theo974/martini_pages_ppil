package fr.FlorentMartini.Main.Parser;

import fr.FlorentMartini.Main.Color;
import fr.FlorentMartini.Main.Requetes.RequeteCercle;

public class ParserRequeteCercle extends ParserRequete {

    public ParserRequeteCercle(ParserRequete suivant) { super(suivant); }

    @Override
    public RequeteCercle parser(String requete) {
        String[] coordonnees = new String[40];
        String[] position = requete.split(";");
        Color couleur = Color.valueOf(position[1]);
        for(int i=2; i<position.length; i++) {
            coordonnees[i-2] = position[i];
        }
        return new RequeteCercle(position[0], couleur, coordonnees);
    }

    @Override
    public boolean saitParser(String requete) { return requete.contains("CERCLE"); }
}
