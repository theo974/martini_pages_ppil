package fr.FlorentMartini.Main;

import fr.FlorentMartini.Main.Parser.ParserRequete;
import fr.FlorentMartini.Main.Parser.ParserRequeteCercle;
import fr.FlorentMartini.Main.Parser.ParserRequetePolygone;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Accueil {
    public static void main(String[] args) {
        //Initialisation du parser
        ParserRequete expert = null;
        expert = new ParserRequetePolygone(expert);
        expert = new ParserRequeteCercle(expert);
        try {
            //Initialisation du serveur
            int portServeur = 5757;
            ServerSocket serveur = null;
            serveur = new ServerSocket(portServeur);
            System.out.println("Serveur prêt : " + serveur);
            int noClient = 1;
            while(true) {
                //Gère les nouvelles connexion et créer un thread pour chacun d'eux
                Socket socket = serveur.accept(); //Pause jusqu'à nouvelle connexion
                System.out.println("Connexion réussie ! Client numéro : " + noClient);
                Interlocuteur interlocuteur = new Interlocuteur(socket, noClient, expert); //Création du thread
                interlocuteur.start();
                noClient++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
