package fr.FlorentMartini.Main;

import fr.FlorentMartini.Main.GUI.MainGUI;
import fr.FlorentMartini.Main.Parser.ParserRequete;
import fr.FlorentMartini.Main.Requetes.Requete;
import fr.FlorentMartini.Main.Requetes.RequetePolygone;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;

public class Interlocuteur extends Thread {

    BufferedReader fluxEntrant;
    PrintStream fluxSortant;
    ParserRequete expert;
    int noClient;

    public Interlocuteur(Socket socket, int noClient, ParserRequete expert) throws IOException {
        this.fluxEntrant = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.fluxSortant = new PrintStream(socket.getOutputStream());
        this.noClient = noClient;
        this.expert = expert;
    }

    @Override
    public void run() {
        //Création du GUI
        MainGUI affichage = new MainGUI();
        affichage.initialize();
        while(!this.isInterrupted()) {
            String requete = null;
            try {
                //Lecture de la requête
                requete = this.fluxEntrant.readLine();
                System.out.println("Le client n° " + this.noClient + " a envoyé : " + requete);
                //Création du tableau de requêtes
                ArrayList<Requete> request = new ArrayList<>();
                //Analyse de la requête
                expert.traiter(requete, request);
                //Exécution de la requête
                System.out.println(request.get(0).toString());
                request.get(0).actionRequete(affichage);
                //Message de validation
                this.fluxSortant.println("Requete prise en compte !");
            } catch (Exception e) {
                this.fluxSortant.println(e.getMessage());
            }
        }
    }
}
