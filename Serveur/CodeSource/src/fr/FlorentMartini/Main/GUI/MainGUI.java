package fr.FlorentMartini.Main.GUI;

import javax.swing.*;
import java.awt.*;

public class MainGUI extends JFrame {
    public void initialize() {
        setTitle("Dessin");
        setSize(1050,1050);
        setMinimumSize(new Dimension(1050, 1050));
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setVisible(true);
    }
}
