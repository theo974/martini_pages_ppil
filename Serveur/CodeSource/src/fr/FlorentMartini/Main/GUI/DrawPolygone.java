package fr.FlorentMartini.Main.GUI;

import fr.FlorentMartini.Main.Requetes.Requete;

import java.awt.*;

public class DrawPolygone extends Draw {
    public DrawPolygone(Requete requete) {
        super(requete);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(new Color(getRequest().getCouleur().getRed(), getRequest().getCouleur().getGreen(), getRequest().getCouleur().getBlue()));
        for(int i=0; i<((getRequest().getNbCotes()*2)-2); i=i+2) {
            g.drawLine(getRequest().getPositions()[i],getRequest().getPositions()[i+1],getRequest().getPositions()[i+2],getRequest().getPositions()[i+3]);
            System.out.println("Ligne crée aux extrémités de coordonnées (" + getRequest().getPositions()[i] + "," + getRequest().getPositions()[i+1] + ") et (" + getRequest().getPositions()[i+2] + "," + getRequest().getPositions()[i+3] + ")");
        }
        if(getRequest().getNbCotes()>2) {
            g.drawLine(getRequest().getPositions()[0],getRequest().getPositions()[1],getRequest().getPositions()[(getRequest().getNbCotes()*2)-2],getRequest().getPositions()[(getRequest().getNbCotes()*2)-1]);
            System.out.println("Ligne crée aux extrémités de coordonnées (" + getRequest().getPositions()[0] + "," + getRequest().getPositions()[1] + ") et (" + getRequest().getPositions()[(getRequest().getNbCotes()*2)-2] + "," + getRequest().getPositions()[(getRequest().getNbCotes()*2)-1] + ")");
        }
    }
}
