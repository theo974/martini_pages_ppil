package fr.FlorentMartini.Main.GUI;

import fr.FlorentMartini.Main.Requetes.Requete;

import java.awt.*;

public class DrawCercle extends Draw {
    public DrawCercle(Requete requete) {
        super(requete);
    }

    @Override
    public void paintComponent(Graphics g) {
        int rayon = getRayon();
        int pointHautGaucheX = (getRequest().getPositions()[0])-rayon;
        int pointHautGaucheY = (getRequest().getPositions()[1])-rayon;
        g.setColor(new Color(getRequest().getCouleur().getRed(), getRequest().getCouleur().getGreen(), getRequest().getCouleur().getBlue()));
        g.drawOval(pointHautGaucheX, pointHautGaucheY, rayon*2, rayon*2);
        System.out.println("Cercle crée, centrée en (" + getRequest().getPositions()[0] + "," + getRequest().getPositions()[1] + ") avec pour rayon " + rayon + ".");
    }

    private int getRayon() {
        int getDifferenceX = Math.abs(getRequest().getPositions()[2] - getRequest().getPositions()[0]);
        int getDifferenceY = Math.abs(getRequest().getPositions()[3] - getRequest().getPositions()[1]);
        int hypothenuse = (int) Math.ceil(Math.sqrt(Math.pow(getDifferenceX, 2) + Math.pow(getDifferenceY, 2)));
        return hypothenuse;
    }
}
