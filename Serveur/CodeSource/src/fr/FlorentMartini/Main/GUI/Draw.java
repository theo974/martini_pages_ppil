package fr.FlorentMartini.Main.GUI;

import fr.FlorentMartini.Main.Requetes.Requete;

import javax.swing.*;
import java.awt.*;

public abstract class Draw extends JPanel {
    private Requete request;

    public Draw(Requete requete) {
        this.request = requete;
    }

    public Requete getRequest() {
        return request;
    }

    @Override
    public abstract void paintComponent(Graphics g);
}
